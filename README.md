# Project "PhoneBook on ProtoBuf"

## Данный репозиторий содержит самостоятельный проект на языке GO 
 
### Условия задачи:
1. Проект должен быть создан на базе COBRA
2. Использовать Protocol Buffers

### Инструкция по создания проекта:
1. Скачать плагин protobuf протокола Go. Ввести в терминале:
<pre>
 go get github.com/golang/protobuf/protoc-gen-go
</pre>
2. Создать проект COBRA. Для этого перейти в консоле в директорию %GOPATH%\bin и набрать команду 
<pre>
cobra init %GOPATH%/[Путь к проекту] (Например: E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookCobraSruct.git)
</pre>
команда создаст исходный код приложения.
3. Создать proto файл в пакете model(gitlab.com\jaroslav.kuchmij\phoneBookCobraSruct.git/model)
Структура файла:
<pre>
syntax = "proto3";
package model;
message Contact {
    string namber = 1;
    string name = 2;
    string address = 3;
}
message Book {
    repeated Contact contacts = 1;
}
</pre>
4. Скачать компилятор protobuf, для этого:
    - перейти по ссылке [gitProtoBuf](https://github.com/google/protobuf/releases)
    - скачать zip архив protoc-3.5.0-win32.zip
    - распаковать и положить папку в $GOPATH/bin
5. В консоле перейти в $GOPATH/bin/protoc-3.5.0-win32/bin и запустить комманду:
<pre>
protoc -I=$GOPATH\src\[имя проекта] --go_out=$GOPATH\src\[имя проекта] $GOPATH\src\[имя проекта]\model\Contact.proto
</pre>
после выполнения, в директории $GOPATH\src\[имя проекта]\model сгенерится структура Contact.pb.go
7. Далее создать COBRA команды используя структуру protoBuf

### Запуск проекта:
1. Скачать проект - 
<pre>
go get gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf.git
</pre>
2. Перейти в терминале в папку  %GOPATH%/src/gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf.git
3. Ввыполнить команду go run main.go <command>:
    - create - создание файла
    - delete - удаление файла
    - addContact - добавление контакта
    - searchContact - поиск контакта по номеру телефона
    - getContacts - получение всего списка контактов
    - updateName - изменение имени по телефону
    - updateAddress - изменение адреса по номеру телефона
    - deleteContact - удаление контакта по номеру телефона

**~~Создатель кода~~** Главный бездельник - [*YaroslavKuchmiy*](https://www.facebook.com/profile.php?id=100006520172879)