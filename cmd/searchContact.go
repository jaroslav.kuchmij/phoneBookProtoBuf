package cmd

import (
	"fmt"
	"gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf.git/utils"
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf.git/model"
)

func init() {
	rootCmd.AddCommand(searchContactCmd)
}

// Команда поиск контакта по номеру телефона
var searchContactCmd = &cobra.Command{
	Use:   "searchContact",
	Short: "search Contact into file",
	Long:  `This command searching Contact into file by phone namber`,
	Run: func(cmd *cobra.Command, args []string) {

	contact := new(model.Contact)

	if utils.CheckIsNotExist() {		//проверка на существование файла
		fmt.Println("File does not exist")
		return
	}
		
	fmt.Println("\t\tSearching")
 	fmt.Print("Enter the phone number (format: +380981111111):")
	fmt.Scanln(&contact.Namber)

	if !utils.CheckValidNamber(contact.Namber) {	//проверка на формат номера
		fmt.Println("Invalid namber")
		return
	}

	if bool, item := utils.SearchNamberInFile(contact.Namber); bool == true { //проверка на существование номера в файле
		utils.PrintStructBook(item)
		return
	}

	fmt.Println("This number does not exist")
	},
}