package cmd

import (
	"fmt"
	"strings"
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf.git/utils"
	"gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf.git/model"
	"log"
)

func init() {
	rootCmd.AddCommand(updateAddressCmd)
}

// Команда изменения адреса контакта по номеру телефона 
var updateAddressCmd = &cobra.Command{
	Use:   "updateAddress",
	Short: "update Address into file",
	Long:  `This command updating Address contact. 
	Searching occurs on phone namber`,
	Run: func(cmd *cobra.Command, args []string) {

		contact := new(model.Contact)

		if utils.CheckIsNotExist() {		//проверка на существование файла
			log.Println("File does not exist")
			return
		}

		fmt.Println("\t\tUpdating address contact")
		fmt.Print("Enter the phone number (format: +380981111111):")
		fmt.Scanln(&contact.Namber)

		if !utils.CheckValidNamber(contact.Namber) {		//проверка на формат номера
			log.Println("Invalid namber")
			return
		}

		book := utils.ReadAndUnmarshalFile() //десериализация и выгрузка в структуру

		for count, item := range book.Contact  { //итерация среза Contact
			if res := strings.Contains(item.Namber, contact.Namber); res { //Проверка на совпадение
				fmt.Println("Enter the new address")
				fmt.Scanln(&item.Address) //если есть совпадение, вводим новое значение
				book.Contact[count] = item  //записываем значение в структуру Contact по индексу
				utils.MarshalAndWriteInFile(book) //сериализация и запись в файл
				log.Println("Updating phone namber")
				return
			}
	    	}
		log.Println("This number does not exist")
	},
}