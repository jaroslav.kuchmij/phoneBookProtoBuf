package cmd

import (
	"fmt"
	"strings"
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf.git/utils"
	"log"
	"gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf.git/model"
)

func init() {
	rootCmd.AddCommand(deleteContactCmd)
}

// Команда удаления контакта по номеру телефона 
var deleteContactCmd = &cobra.Command{
	Use:   "deleteContact",
	Short: "delete contact into file",
	Long:  `This command deleting contact. 
	Searching occurs on phone namber`,
	Run: func(cmd *cobra.Command, args []string) {

		contact := new(model.Contact)

		if utils.CheckIsNotExist() {		//проверка на существование файла
			log.Println("File does not exist")
			return
		}

		fmt.Println("\t\tUpdating address contact")
		fmt.Print("Enter the phone number (format: +380981111111):")
		fmt.Scanln(&contact.Namber)

		if !utils.CheckValidNamber(contact.Namber) {		//проверка на формат номера
			log.Println("Invalid namber")
			return
		}

		book := utils.ReadAndUnmarshalFile() //десериализация и выгрузка в структуру

		for count, item := range book.Contact  { //итерация среза Contact
			if res := strings.Contains(item.Namber, contact.Namber); res { //проверка на совпадение
				copy(book.Contact[count:], book.Contact[count+1:])  //копируем следующий элемент массива на место найденной строки
				book.Contact[len(book.Contact)-1] = nil 		//обнуляем "хвост"
				book.Contact = book.Contact[:len(book.Contact)-1] 		//запись получнного массива
				utils.MarshalAndWriteInFile(book) //cериализация и запись в файл
				log.Println("Deleting phone namber")
				return
			}
		}
		log.Println("This number does not exist")
	},
}