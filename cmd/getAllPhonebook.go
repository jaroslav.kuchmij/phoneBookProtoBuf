package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf.git/utils"
	"log"
)

func init() {
	rootCmd.AddCommand(getContactsCmd)
}

// Команда чтения всех элементов из файла
var getContactsCmd = &cobra.Command{
	Use:   "getContacts",
	Short: "get all contacts of file",
	Long:  `This command reads items into file and print this items on the screen`,
	Run: func(cmd *cobra.Command, args []string) {

		// проверка на существование файла
		if utils.CheckIsNotExist() {
			log.Fatal("File does not exist")
			return
		}

		//десериализация файла
		book := utils.ReadAndUnmarshalFile()

		//вывод контактов на экран
		for _, contact := range book.Contact {
			utils.PrintStructBook(contact)
		}
	},
}