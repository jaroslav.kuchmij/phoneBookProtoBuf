package cmd

import (
        "gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf.git/model"
        "github.com/spf13/cobra"
        "log"
        "gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf.git/utils"
        "fmt"
)

func init() {
        rootCmd.AddCommand(addContactCmd)
}

var addContactCmd = &cobra.Command{
        Use:   "addContact",
        Short: "add contact from file",
        Long:  `This command add phone namber, name and address into file.

	Format phone: +380111111111
	Format Name and Address: free form
	Example: +380984001650|Yaroslav|Voenomorskaya str.`,
        Run: func(cmd *cobra.Command, args []string) {

                contact := new(model.Contact)

                // проверка на существование файла
                if utils.CheckIsNotExist() {
                        log.Fatal("File does not exist")
                        return
                }
                err := setContactInStruct(contact) //Запись в струткруру значений
                if err != 0 {
                        return
                }
                setContactInFile(contact) //Запись полученных данных в файл
        },
}

//Запись в структуру Contact данных введенных через терминал
func setContactInStruct(contact *model.Contact) int  {

        fmt.Println("Enter the phone number (format: +380981111111):")
        fmt.Scanln(&contact.Namber) //через терминал передаем значение в поле структуры Contact

        //проверка на формат номера
        if !utils.CheckValidNamber(contact.Namber) {
                log.Printf("Invalid namber")
                return 1
        }

        //проверка на существование номера в файле.
        if bool, item := utils.SearchNamberInFile(contact.Namber); bool == true {
                log.Println("This number exists")
		utils.PrintStructBook(item)
                return 1
        }


        fmt.Println("Enter Name:")
        fmt.Scanln(&contact.Name) //через терминал передаем значение в поле структуры Contact

        fmt.Println("Enter address:")
        fmt.Scanln(&contact.Address) //через терминал передаем значение в поле структуры Contact

        return 0
}

//Запись переданных данных в файл
func setContactInFile (contact *model.Contact) {
        //десериализация и выгрузка в структуру
        book := utils.ReadAndUnmarshalFile()
        book.Contact = append(book.Contact, contact) //добавление к срезу переданные парамметры
	//Сериализация и запись в файл
	utils.MarshalAndWriteInFile(book)
        return
}