package cmd

import (
	"os"
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf.git/utils"
	"log"
)

func init() {
	rootCmd.AddCommand(createFileCmd)
}

// 
var createFileCmd = &cobra.Command{
	Use:   "create",
	Short: "create File",
	Long:  `This command creates a file`,
	Run: func(cmd *cobra.Command, args []string) {
		
	if utils.CheckIsNotExist() {		//проверка на существование файла
		file, err := os.Create(utils.NameFile) //Создание файла
		if err != nil {
			return
		}
		defer file.Close() //Отложенное закрытие фала
		log.Println("Created file")
		return
	}

	log.Println("The file already exists!")
	},
}