package cmd

import (
	"os"
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf.git/utils"
	"log"
)

func init() {
	rootCmd.AddCommand(deleteFileCmd)
}

// Команда удаления файла
var deleteFileCmd = &cobra.Command{
	Use:   "delete",
	Short: "delete File",
	Long:  `This command delete a file`,
	Run: func(cmd *cobra.Command, args []string) {
		
	if !utils.CheckIsNotExist() {		//проверка на существование файла
		os.Remove(utils.NameFile) //удаление файла
		log.Println("File deleted")
		return
	}
	log.Println("The file does not exist!")
	},
}