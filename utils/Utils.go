package utils

import (
	"os"
	"regexp"
	"io/ioutil"
	"strings"
	"gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf.git/model"
	"fmt"
	"github.com/golang/protobuf/proto"
	"log"
)

const NameFile = "phoneBook.txt"

//Десириализация
func ReadAndUnmarshalFile() *model.Book  {
	book := &model.Book{}

	temp, err := ioutil.ReadFile(NameFile)
	if err != nil{
		panic(err)
	}

	err = proto.Unmarshal(temp, book)
	if err != nil{
		panic(err)
	}
	return book
}

//Сириализация
func MarshalAndWriteInFile(book *model.Book)  {
	out, err := proto.Marshal(book) //перевод Структуры в формат ProtoBuf (сирилизация)
	if err != nil{
		log.Fatal("marshal error: ", err)
		return
	}
	err = ioutil.WriteFile(NameFile, out, 0644) //запись сирилизованных данных в файл
	if err != nil{
		log.Fatal("write file error: ", err)
		return
	}
}

//Поиск совпадений по номеру в файле
func SearchNamberInFile(incomingValue string)  (bool, *model.Contact) {
	book := ReadAndUnmarshalFile()

	for _, item := range book.Contact {
		//проверка входящего значения на совпадения в файле
		if res := strings.Contains(item.Namber, incomingValue); res {
			return true, item
		}
	}
	return false, nil
}

//Проверка на существование файла
func CheckIsNotExist() bool {
	var _, err = os.Stat(NameFile)
	return os.IsNotExist(err)
}

//Проверка входящего номера на корректность
func CheckValidNamber(incomingValue string) bool {
	var validID = regexp.MustCompile(`^[+][3][8][0][0-9\\s\\(\\)\\+]{9}$`) //формат номера
	res := validID.MatchString(incomingValue)
	return res
}

//Вывод структуры телефонной книги на экран
func PrintStructBook(contact *model.Contact)  {
	fmt.Println("Contact namber: ", contact.Namber)
	fmt.Println("Contact name: ", contact.Name)
	fmt.Println("Contact address: ", contact.Address)
	fmt.Println("-------------------------------------------------------")
}